from django.db import models
from PIL import Image
from django.utils import timezone
from django.template.defaultfilters import truncatewords  # or truncatechars
# Create your models here.


class Category(models.Model):
    class Meta:
        verbose_name_plural = "Categories"
    # Categories of Navbar
    icon = models.ImageField(
        upload_to="images/", blank=True, null=True)
    name = models.CharField(max_length=255)
    link = models.CharField(max_length=2048)

    def __str__(self):
        return "{}".format(self.name)


class Logo(models.Model):
    icon = models.ImageField(upload_to='images/')
    name = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.name)


class Slider(models.Model):
    # One banner for slider
    web_image = models.ImageField(upload_to='images/slider')
    mobile_image = models.ImageField(upload_to='images/slider')
    # there are limited space in frontend
    title = models.CharField(max_length=500)
    button = models.CharField(max_length=100)
    link = models.CharField(max_length=2048)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.title)


class Statistics(models.Model):
    class Meta:
        verbose_name_plural = "Statistics"

    # Statistics for OurExperience
    icon = models.ImageField(upload_to="images/")
    title = models.CharField(max_length=500)
    number = models.PositiveIntegerField()
    description = models.CharField(max_length=1000)
    is_active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now=True, blank=True)

    # to truncate words that displays in ADMIN Panel
    # @property
    # def short_description(self):
    #     return truncatewords(self.description, 15)

    def __str__(self):
        return "Statistics"


# class OurExperience(models.Model):
#     title = models.CharField(
#         max_length=255, default=None, blank=True, null=True)
#     description = models.CharField(
#         max_length=1000, default=None, blank=True, null=True)
#     statistics = models.ForeignKey(Statistics, on_delete=models.CASCADE)

#     def __str__(self):
#         return "Our Experience {}".format(self.description)


class MoreAboutUs(models.Model):
    class Meta:
        verbose_name_plural = "More About Us"
    title = models.CharField(max_length=500)
    # video or image link
    link = models.CharField(max_length=2048, blank=True, null=True)
    thumbnail = models.ImageField(
        upload_to="images/works", blank=False, null=False)
    description = models.CharField(max_length=5000, blank=True, null=True)
    button = models.CharField(max_length=2048, blank=True, null=True)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.title)


class Work(models.Model):
    # for describe what we do
    icon = models.ImageField(upload_to="images/", blank=True, null=True)
    title = models.CharField(max_length=500)
    link = models.CharField(max_length=2048, blank=True, null=True)
    button = models.CharField(max_length=100, blank=True, null=True)
    belongs = models.ForeignKey(MoreAboutUs, on_delete=models.CASCADE, related_name="works")

    def __str__(self):
        return "{}".format(self.title)


# class WhatWeDo(models.Model):
#     title = models.CharField(max_length=500)
#     works = models.ForeignKey(Work, on_delete=models.CASCADE)

#     def __str__(self):
#         return "What we do - {}".format(self.title)


class ProductDetail(models.Model):
    class Meta:
        verbose_name_plural = "Products"
    logo = models.ImageField(upload_to="images/")
    image = models.ImageField(upload_to="images/products")
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=1000, blank=True, null=True)
    link = models.CharField(max_length=2048, blank=True, null=True)
    button = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return "{}".format(self.name)


class Banner(models.Model):
    name = models.CharField(max_length=500)
    web_image = models.ImageField(upload_to="images/banners")
    mobile_image = models.ImageField(upload_to="images/banners")
    link = models.CharField(max_length=2048, blank=True, null=True)
    description = models.TextField()
    button = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.name)


class Certificate(models.Model):
    name = models.CharField(max_length=500)
    image = models.ImageField(upload_to='images/certificates')
    description = models.CharField(max_length=5000)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.name)


class News(models.Model):
    class Meta:
        verbose_name_plural = "News"

    title = models.CharField(max_length=500)
    description = models.TextField()
    link = models.CharField(max_length=2048, blank=True, null=True)
    button = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return "News - {}".format(self.title)


class NewsImage(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField( blank=True, null=True)
    image = models.ImageField(upload_to="images/news")
    belongs_to = models.ForeignKey(
        News, on_delete=models.CASCADE, related_name="news")


class Gallery(models.Model):
    class Meta:
        verbose_name_plural = "Gallery"

    title = models.CharField(max_length=500, blank=True, null=True)
    # image = models.ForeignKey(Image, on_delete=models.CASCADE)
    description = models.CharField(max_length=1000, blank=True, null=True)
    button = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return "Gallery - {}".format(self.title)


class GalleryImage(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to="images/gallery")
    is_web = models.BooleanField(default=True)
    belongs_to = models.ForeignKey(
        Gallery, on_delete=models.CASCADE, related_name="gallery")


class ContactUs(models.Model):
    class Meta:
        verbose_name_plural = "Contact Us"
    name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    interest = models.CharField(max_length=1000)

    def __str__(self):
        return "Contact us - {}".format(self.name)


class AboutUs(models.Model):
    class Meta:
        verbose_name_plural = "About Us"

    phone_number = models.CharField(max_length=100)
    email = models.CharField(max_length=255)
    address = models.CharField(max_length=2000)
    web_image = models.ImageField(upload_to="images/map")
    mobile_image = models.ImageField(upload_to="images/map")
    # can be the location of map, lantitute and longtitude
    location = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return "{}".format(self.email)


class Contacts(models.Model):
    class Meta:
        verbose_name_plural = "Contacts"
    phone_numbers = models.CharField(max_length=1000)
    emails = models.CharField(max_length=1000)
    address = models.CharField(max_length=1000)

    def __str__(self):
        return "{}".format(self.address)


class AboutCompany(models.Model):
    class Meta:
        verbose_name_plural = "About Company"
    logo = models.ImageField(upload_to="images/logo")
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=1000)

    def __str__(self):
        return "About Company - {}".format(self.name)


class Partner(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    logo = models.ImageField(upload_to="images/partners")
    links = models.CharField(max_length=2048)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return "Partner - {}".format(self.name)
