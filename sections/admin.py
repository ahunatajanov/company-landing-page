from django.contrib import admin
from django.utils.html import format_html
from .models import (
    Category, Logo, Slider, Statistics, MoreAboutUs, Work, ProductDetail,
    Banner, Certificate, NewsImage, News, GalleryImage, Gallery, ContactUs, AboutUs,
    Contacts, AboutCompany, Partner)

# Register your models here.
admin.site.site_header = "Takyk Ulgam"
admin.site.site_title = "Takyk Ulgam Portal"
admin.site.index_title = "Welcome to Takyk Ulgam Portal"


class CategoryAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):

        if obj.icon:
            temp = '<img src="{}" height="20rem" />'.format(obj.icon.url)
            return format_html(temp)
        return None

    image_tag.short_description = 'Icon'
    list_display = ("id", "image_tag", "name", "link")


class LogoAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.icon.url))

    image_tag.short_description = 'Icon'
    list_display = ("id", "image_tag", "name")


class SliderAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.web_image.url))

    image_tag.short_description = 'Image'
    list_display = ("image_tag", "title", "button", "link", "is_active")
    list_display_links = ("image_tag", "title", "button", "link")
    list_editable = ("is_active", )


class StatisticsAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.icon.url))
    image_tag.short_description = 'Icon'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("image_tag", "title", "number",
                    "desc", "created", "updated", "is_active")
    list_display_links = ("image_tag", "title", "number",
                          "desc", "created", "updated")
    list_editable = ("is_active", )

class WorkInline(admin.TabularInline):
    model = Work
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.icon.url))
    image_tag.short_description = 'Icon'

    list_display = ("image_tag", "title", "link",
                    "button")
    list_display_links = ("image_tag", "title", "link",
                          "button")



class MoreAboutUsAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False
    inlines = (WorkInline, )

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.thumbnail.url))
    image_tag.short_description = 'Thumbnail'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("image_tag", "title", "link",
                    "desc", "button", "is_active")
    list_display_links = ("image_tag", "title", "link",
                          "desc", "button")
    list_editable = ("is_active", )



class ProductDetailAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag1(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.logo.url))
    image_tag1.short_description = 'Logo'

    def image_tag2(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.image.url))
    image_tag2.short_description = 'Image'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:80])
    desc.short_description = "Description"

    list_display = ("image_tag1", "image_tag2", "name", "desc",
                    "created", "updated")
    list_display_links = ("image_tag1", "image_tag2", "name", "desc",
                          "created", "updated")


class BannerAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.web_image.url))
    image_tag.short_description = 'Image'

    list_display = ("image_tag", "name", "is_active")
    list_display_links = ("image_tag", "name")
    list_editable = ("is_active", )


class CertificateAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.image.url))
    image_tag.short_description = 'Image'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("image_tag", "name", "desc", "is_active")
    list_display_links = ("image_tag", "name", "desc")
    list_editable = ("is_active", )


class NewsAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.image.url))
    image_tag.short_description = 'Image'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("image_tag", "title", "desc",
                    "created", "updated", "is_active")
    list_display_links = ("image_tag", "title", "desc", "created", "updated")
    list_editable = ("is_active", )


class GalleryImageAdminInline(admin.TabularInline):
    model = GalleryImage


class GalleryAdmin(admin.ModelAdmin):
    inlines = (GalleryImageAdminInline, )
    actions_on_bottom = True
    actions_on_top = False

    # def image_tag(self, obj):
    #     return format_html('<img src="{}" height="30rem" />'.format(obj.image.url))
    # image_tag.short_description = 'Image'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("title", "desc",
                    "created", "updated", "is_active")
    list_display_links = ("title", "desc",
                          "created", "updated")
    list_editable = ("is_active", )


class NewsImageInline(admin.TabularInline):
    model = NewsImage


class NewsAdmin(admin.ModelAdmin):
    inlines = (NewsImageInline, )
    actions_on_bottom = True
    actions_on_top = False

    # def image_tag(self, obj):
    #     return format_html('<img src="{}" height="30rem" />'.format(obj.image.url))
    # image_tag.short_description = 'Image'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("title", "desc",
                    "created", "updated", "is_active")
    list_display_links = ("title", "desc", "created", "updated")
    list_editable = ("is_active", )


class ContactUsAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    list_display = ("name", "phone_number", "interest")
    list_display_links = ("name", "phone_number", "interest")


class AboutUsAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.web_image.url))
    image_tag.short_description = 'Image'

    list_display = ("image_tag", "phone_number", "email",
                    "address")
    list_display_links = ("image_tag", "phone_number", "email",
                          "address")


class ContactsAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    list_display = ("phone_numbers", "emails", "address")
    list_display_links = ("phone_numbers", "emails", "address")


class AboutCompanyAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.logo.url))
    image_tag.short_description = 'Logo'

    # truncated description
    def desc(self, obj):
        return "{}...".format(obj.description[:100])
    desc.short_description = "Description"

    list_display = ("image_tag", "name", "desc")
    list_display_links = ("image_tag", "name", "desc")


class PartnerAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    actions_on_top = False

    def image_tag(self, obj):
        return format_html('<img src="{}" height="30rem" />'.format(obj.logo.url))
    image_tag.short_description = 'Logo'

    list_display = ("image_tag", "name", "is_active")
    list_display_links = ("image_tag", "name")
    list_editable = ("is_active", )


admin.site.register(Category, CategoryAdmin)
admin.site.register(Logo, LogoAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(Statistics, StatisticsAdmin)
admin.site.register(MoreAboutUs, MoreAboutUsAdmin)
# admin.site.register(Work, WorkInline)
admin.site.register(ProductDetail, ProductDetailAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(Certificate, CertificateAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Gallery, GalleryAdmin)
admin.site.register(ContactUs, ContactUsAdmin)
admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Contacts, ContactsAdmin)
admin.site.register(AboutCompany, AboutCompanyAdmin)
admin.site.register(Partner, PartnerAdmin)
