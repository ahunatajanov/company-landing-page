# Generated by Django 4.1 on 2023-08-04 07:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("sections", "0006_alter_galleryimage_description")]

    operations = [
        migrations.RenameField(
            model_name="moreaboutus", old_name="mobile_thumbnail", new_name="thumbnail"
        ),
        migrations.RemoveField(model_name="moreaboutus", name="web_thumbnail"),
    ]
