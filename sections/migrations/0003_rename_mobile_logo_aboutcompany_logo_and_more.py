# Generated by Django 4.1 on 2023-08-04 07:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("sections", "0002_rename_mobile_icon_logo_icon_remove_logo_web_icon")
    ]

    operations = [
        migrations.RenameField(
            model_name="aboutcompany", old_name="mobile_logo", new_name="logo"
        ),
        migrations.RemoveField(model_name="aboutcompany", name="web_logo"),
    ]
