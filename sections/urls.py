
from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'category', views.CategoryViewSet, basename="category")
router.register(r'logo', views.LogoViewSet, basename="logo")
router.register(r'slider', views.SliderViewSet, basename="slider")
router.register(r'statistics', views.StatisticsViewSet, basename="statistics")
router.register(r'more-about-us', views.MoreAboutUsViewSet,
                basename="more-about-us")
router.register(r'work', views.WorkViewSet, basename="work")
router.register(r'product', views.ProductDetailViewSet, basename="product")
router.register(r'banner', views.BannerViewSet, basename="banner")
router.register(r'certificate', views.CertificateViewSet,
                basename="certificate")
router.register(r'news-image', views.NewsImageViewSet, basename="news-image")
router.register(r'news', views.NewsViewSet, basename="news")
router.register(r'gallery', views.GalleryViewSet, basename="gallery")
router.register(r'contact-us', views.ContactUsViewSet, basename="contact-us")
router.register(r'about-us', views.AboutUsViewSet, basename="about-us")
router.register(r'contacts', views.ContactsViewSet, basename="contacts")
router.register(r'about-company', views.AboutCompanyViewSet,
                basename="about-company")
router.register(r'partner', views.PartnerViewSet, basename="partner")


urlpatterns = [
    path(r'', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
