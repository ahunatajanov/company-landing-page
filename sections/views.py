from rest_framework import viewsets
from .serializers import (
    CategorySerializer, LogoSerializer, SliderSerializer, StatisticsSerializer,
    MoreAboutUsSerializer, WorkSerializer, ProductDetailSerializer, BannerSerializer,
    CertificateSerializer, NewsSerializer, NewsImageSerializer, GallerySerializer, ContactUsSerializer, AboutUsSerializer, ContactsSerializer, AboutCompanySerializer, PartnerSerializer)
from .models import (
    Category, Logo, Slider, Statistics, MoreAboutUs, Work, ProductDetail,
    Banner, Certificate, News, NewsImage, Gallery, ContactUs, AboutUs, Contacts, AboutCompany,
    Partner)

# Create your views here.


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by("id")
    serializer_class = CategorySerializer


class LogoViewSet(viewsets.ModelViewSet):
    queryset = Logo.objects.all().order_by("id")
    serializer_class = LogoSerializer


class SliderViewSet(viewsets.ModelViewSet):
    queryset = Slider.objects.all().order_by("id")
    serializer_class = SliderSerializer


class StatisticsViewSet(viewsets.ModelViewSet):
    queryset = Statistics.objects.all().order_by("id")
    serializer_class = StatisticsSerializer


class MoreAboutUsViewSet(viewsets.ModelViewSet):
    queryset = MoreAboutUs.objects.all().order_by("-id")
    serializer_class = MoreAboutUsSerializer


class WorkViewSet(viewsets.ModelViewSet):
    queryset = Work.objects.all().order_by("id")
    serializer_class = WorkSerializer


class ProductDetailViewSet(viewsets.ModelViewSet):
    queryset = ProductDetail.objects.all().order_by("id")
    serializer_class = ProductDetailSerializer


class BannerViewSet(viewsets.ModelViewSet):
    queryset = Banner.objects.all().order_by("id")
    serializer_class = BannerSerializer


class CertificateViewSet(viewsets.ModelViewSet):
    queryset = Certificate.objects.all().order_by("id")
    serializer_class = CertificateSerializer

class NewsImageViewSet(viewsets.ModelViewSet):
    queryset = NewsImage.objects.all().order_by("-id")
    serializer_class = NewsImageSerializer

class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by("id")
    serializer_class = NewsSerializer


class GalleryViewSet(viewsets.ModelViewSet):
    queryset = Gallery.objects.all().order_by("id")
    serializer_class = GallerySerializer


class ContactUsViewSet(viewsets.ModelViewSet):
    queryset = ContactUs.objects.all().order_by("id")
    serializer_class = ContactUsSerializer


class AboutUsViewSet(viewsets.ModelViewSet):
    queryset = AboutUs.objects.all().order_by("id")
    serializer_class = AboutUsSerializer


class ContactsViewSet(viewsets.ModelViewSet):
    queryset = Contacts.objects.all().order_by("id")
    serializer_class = ContactsSerializer


class AboutCompanyViewSet(viewsets.ModelViewSet):
    queryset = AboutCompany.objects.all().order_by("id")
    serializer_class = AboutCompanySerializer


class PartnerViewSet(viewsets.ModelViewSet):
    queryset = Partner.objects.all().order_by("id")
    serializer_class = PartnerSerializer
