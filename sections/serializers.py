from rest_framework import serializers
from sections.models import (
    Category, Logo, Slider, Statistics, MoreAboutUs, Work, ProductDetail,
    Banner, Certificate, News, NewsImage, Gallery, ContactUs, AboutUs,
    Contacts, AboutCompany, Partner)


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = "__all__"


class LogoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Logo
        fields = "__all__"


class SliderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Slider
        fields = "__all__"


class StatisticsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Statistics
        fields = "__all__"


class WorkSerializer(serializers.ModelSerializer):

    class Meta:
        model = Work
        fields = "__all__"

class MoreAboutUsSerializer(serializers.ModelSerializer):
    works = WorkSerializer(many=True)
    class Meta:
        model = MoreAboutUs
        fields = "__all__"

class ProductDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductDetail
        fields = "__all__"


class BannerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Banner
        fields = "__all__"


class CertificateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Certificate
        fields = "__all__"

class NewsImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = NewsImage
        fields = "__all__"

class NewsSerializer(serializers.ModelSerializer):
    news = NewsImageSerializer(many=True)
    class Meta:
        model = News
        fields = "__all__"


class GallerySerializer(serializers.ModelSerializer):

    class Meta:
        model = Gallery
        fields = "__all__"


class ContactUsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ContactUs
        fields = "__all__"


class AboutUsSerializer(serializers.ModelSerializer):

    class Meta:
        model = AboutUs
        fields = "__all__"


class ContactsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contacts
        fields = "__all__"


class AboutCompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = AboutCompany
        fields = "__all__"


class PartnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Partner
        fields = "__all__"
